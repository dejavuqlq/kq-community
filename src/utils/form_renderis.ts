// 内容管理公共数据资源配置
export default {
  // 一 首页导读
  items: [// 筛选表单渲染
      {
          title: '输入框:',
          type: 'input',
          key: 'inputV',
          placeholder: '请输入内容',
      },
      {
          title: '下拉选择:',
          type: 'select',
          options:[
          {value: 0, label: '选项a'},
          {value: 1, label: '选项b'},
          {value: 2, label: '选项c'},
          ],
          key: 'selectV',
          placeholder: '请选择',
      },
      {
          title: '开关:',
          type: 'switch',
          key: 'switchV',
          activetext: '按月付费',
          intext: '按年付费',
      },
      {
          title: '多选:',
          type: 'checkbox',
          options: ['上海', '北京', '广州', '深圳'],
          key: 'checkboxV',
      },
      {
          title: '日期选择:',
          type: 'date',
          key: 'dateV',
      },
      {
          type: 'button',
          icon: 'el-icon-search',
          title: '搜索'
      },
  ],

  listQuery: {// 筛选表单提交
    page: 1,
    limit: 20,
    inputV: '',
    selectV: 2,
    switchV: false,
    checkboxV: [ '广州', '深圳'],
    dateV: [new Date(2000, 10, 10, 10, 10), new Date(2000, 10, 11, 10, 10)],
  },

  renderForms: [// 编辑/新增 表单渲染
        {
            title: '输入框:',
            type: 'input',
            inputType: 'number',
            key: 'inputV',
            placeholder: '请输入内容',
            maxlength: 2,
        },
      {
          title: '多行输入框:',
          type: 'input',
          inputType: 'textarea',
          key: 'textareaV',
          placeholder: '请输入内容',
          maxlength: 2,
      },
      {
          title: '数字计数器:',
          type: 'inputNumber',
          key: 'inputNumberV',
          placeholder: '请输入内容',
          maxlength: 2,
      },
      {
          title: 'rate评分:',
          type: 'rate',
          key: 'rateV',
          allowHalf: true,
      },
      {
          title: '下拉选择:',
          type: 'select',
          options:[
          {value: 0, label: '选项a'},
          {value: 1, label: '选项b'},
          {value: 2, label: '选项c'},
          ],
          key: 'selectV',
          placeholder: '请选择',
      },
      {
          title: '开关:',
          type: 'switch',
          key: 'switchV',
          activetext: '按月付费',
          intext: '按年付费',
      },
      {
          title: '多选:',
          type: 'checkbox',
          options: ['上海', '北京', '广州', '深圳'],
          key: 'checkboxV',
      },
      {
          title: '时间:',
          type: 'time',
          key: 'timeV',
      },
      {
          title: '日期:',
          type: 'date',
          key: 'dateV',
      },
      {
          title: '日期范围:',
          type: 'dateTime',
          key: 'dateTimeV',
      },
      {
          title: '上传:',
          type: 'upload',
          limit: 2,//上传限制
          multiple: true,//多选
          key: 'uploadV',
          fileUrl: 'http://kq-community.file.zhhost.top/line/',
          fileList: [],
      },
  ],

  form: {// 编辑/新增 表单提交
    inputV: '',
    inputNumberV: '',
    textareaV: '',
    rateV: 2,
    selectV: 2,
    switchV: false,
    checkboxV: [ '广州', '深圳'],
    timeV: '',
    dateV: '',
    dateTimeV: [new Date(2000, 10, 10, 10, 10), new Date(2000, 10, 11, 10, 10)],
    uploadV: []
  },
  
  rules: {// 编辑/新增 表单验证规则
      inputV: [{ required: true, message: '请输入内容', trigger: 'change' }],
      textareaV: [{ required: true, message: '请输入内容', trigger: 'change' }],
      checkboxV: [{ required: true, message: '请至少选择一项', trigger: 'change' }]
  },
  
  // 一 疗休养评论
  comScreenRenders: [// 筛选表单渲染
      {
          type: 'button',
          key: "add",
          icon: 'el-icon-plus',
          title: '新增',
          btnType: 'primary'
      },
      {
          type: 'button',
          key: "delete",
          icon: 'el-icon-delete',
          title: '删除',
          btnType: 'danger'
      },
  ],

  comScreenQuery: {// 筛选表单提交
    page: 1,
    limit: 20,
    inputV: '',
    selectV: 2,
    switchV: false,
    checkboxV: [ '广州', '深圳'],
    dateV: [new Date(2000, 10, 10, 10, 10), new Date(2000, 10, 11, 10, 10)],
  },
}