// 配置文件域名
export const fileHost = () => {
  return "http://kq-community.file.zhhost.top/"
};
// 配置文件域名-oss
export const ossHost = () => {
  return 'http://kq-community.file.zhhost.top/'
};
// 本地未上传地址
export const beforeUpload = () => {
  return "http://kq-community.server.zhhost.top/"
};
// 配置文件域名
export const uploadHost = () => {
  return "http://kq-community.server.zhhost.top/zh8848/upload"
};
// 图片临时回显地址
export const imagesDisplayHost = () => {
    return "http://kq-community.server.zhhost.top/upload/"
};
// 社工照片显示
export const usersrImg = () => {
  return "http://kq-community.file.zhhost.top/users/"
}
// 工单图片显示
export const jobsImg = () => {
  return "http://kq-community.file.zhhost.top/jobs/"
}
// banner图片显示
export const bannerImg = () => {
  return "http://kq-community.file.zhhost.top/banner/"
}
// 纠纷图片显示
export const disputeImg = () => {
  return "http://kq-community.file.zhhost.top/dispute/"
}
// 维修图片显示
export const propertyImg = () => {
  return "http://kq-community.file.zhhost.top/property/"
}
// 疫苗 医疗 服务预约图片显示
export const appointmentsImg = () => {
  return "http://kq-community.file.zhhost.top/appointments/"
}
// 活动图片显示
export const activityImg = () => {
  return "http://kq-community.file.zhhost.top/activity/"
}
// 党建图片显示
export const partyImg = () => {
  return "http://kq-community.file.zhhost.top/party/"
}

// 配置 社工等级1
export const leaves1 = () => {
  return [
    {id: '0', title: '工作人员'},
    {id: '1', title: '普通党员'},
    {id: '2', title: '主任'},
    {id: '3', title: '委员'},
    {id: '4', title: '副书记'},
    {id: '5', title: '书记'},
  ]
}

// 配置 社工等级
export const leaves = () => {
  return {
    "0": [{id: '-1', title: '普通居民'}],//0-居民
    "1": [
      {id: '0', title: '工作人员'},
      {id: '1', title: '普通党员'},
      {id: '2', title: '主任'},
      {id: '3', title: '委员'},
      {id: '4', title: '副书记'},
      {id: '5', title: '书记'},
    ],//1-社工
    "2": [//2-领导
      {id: '1', title: '普通党员'},
      {id: '2', title: '主任'},
      {id: '3', title: '委员'},
      {id: '4', title: '副书记'},
      {id: '5', title: '书记'},
    ],
  }
}
// 配置 身份
export const isworkersType = () => {
  return [
    {id: '0', title: '居民'},
    {id: '1', title: '社工'},
    {id: '2', title: '领导'},
  ]
}
// 配置 身份2
export const isworkersType2 = () => {
  return [
    {id: '1', title: '社工'},
    {id: '2', title: '领导'},
  ]
}

// 快捷时间选项
export const shortcuts = () => {
  return [{
    text: '最近一周',
    value: (() => {
      const end = new Date();
      const start = new Date();
      start.setTime(start.getTime() - 3600 * 1000 * 24 * 7);
      return [start, end]
    })()
  }, {
    text: '最近一个月',
    value: (() => {
      const end = new Date();
      const start = new Date();
      start.setTime(start.getTime() - 3600 * 1000 * 24 * 30);
      return [start, end]
    })()
  }, {
    text: '最近三个月',
    value: (() => {
      const end = new Date();
      const start = new Date();
      start.setTime(start.getTime() - 3600 * 1000 * 24 * 90);
      return [start, end]
    })()
  }]
};

// 设置cookie
export const setCookie = (name: string, value: string, day: number = 30) => {
  let Days = 30;
  let exp = new Date();
  exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
  document.cookie = name + "=" + escape(value) + ";expires=" + exp.toUTCString();
};

// 获取cookie
export const getCookie = (name: string) => {
  let prefix = name + "="
  let start = document.cookie.indexOf(prefix)
  if (start == -1) {
    return null;
  }
  let end = document.cookie.indexOf(";", start + prefix.length)
  if (end == -1) {
    end = document.cookie.length;
  }
  let value = document.cookie.substring(start + prefix.length, end)
  return unescape(value);
};

// 删除cookie
export const delCookie = (name: string) => {
  let arr;
  let reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
  // eslint-disable-next-line no-cond-assign
  if (arr = document.cookie.match(reg))
    return unescape(arr[2]);
  else
    return null;
};

// 获取地址栏参数
export const setectQuery = (name: string = '') => {
  let href: string = window.location.href;
  let strArr: Array<string> = href.split("?");
  if (strArr.length === 0) {
    return "没有参数"
  } else if (strArr.length > 2) {
    return "Error:发现多个 ？"
  } else {
    let queryArr: Array<string> = strArr[1].split("&"),
        obj: any = {};
    queryArr.map((item: string, index: number) => {
      let o: Array<string> = item.split("=");
      obj[o[0]] = o[1]
    })
    if (name) {
      return obj[name]
    } else {
      return obj
    }
  }
};

// 日期格式化
export const formatDate = (date: string = '') => {
  if (date) {
    let newDate = new Date(date);

    let year = newDate.getFullYear().toString();
    let month = (newDate.getMonth() + 1).toString();
    let day = newDate.getDate().toString();
    if (month.length < 2) {
      month = '0' + month;
    }
    if (day.length < 2) {
      day = '0' + day;
    }
    return `${year}-${month}-${day}`;
  } else {
    return ''
  }
};

// 与运算 数组转int arr的数组必须是2的倍数
export const arrToInt = (arr: Array<number>) => {
  if (arr.length === 0) {
    return {val: '-1', msg: '数组为空'}
  } else {
    // 判断数组是否为2的倍数
    let flag = true;
    arr.map((item) => {
        let f = check(item);
        if(!f){
          flag = false;
        }
    });
    if (flag) {
      let o = arr[0];
      for (let i = 1; i < arr.length; i++) {
        o = o | arr[i]
      }
      return {val: '0', msg: o}
    } else {
      return {val: '-1', msg: '数组某项值不是2的n次幂，请检查数组'}
    }
  }

};

export const check=(num:number)=>{
  if(num != 1){
    while(num != 1){
      if(num%2 == 0){
        num = num / 2;
      }else{
        return false;
      }
    }
    return true;
  }else{
    return true;
  }
}

// 反向 & 0代表false 非0代表true //*处理逻辑 传入原数组 逐个对比 非0代表选中 [arr的规则]
export const intToArr = (num:number,arr:Array<number>)=>{
  let newArr:Array<number>=[];
  if(arr.length>0){
    // 判断数组是否为2的倍数
    let flag = true;
    arr.map((item) => {
      let f = check(item);
      if(!f){
        flag = false;
      }
    });
    if (flag) {
      arr.map((item) => {
        if((num & item)!==0){
          newArr.push(item);
        }
      });
      return {val: '0', msg: newArr }
    } else {
      return {val: '-1', msg: '数组某项值不是2的n次幂，请检查数组'}
    }
  }else{
    return {val:"-1",msg:'数组为空'};
  }
};


