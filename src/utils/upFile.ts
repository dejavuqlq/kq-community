/*
 * dejavu 2021-09-29
 *
 * upFile(accept,multiple,url);
 *
 * accept(上传类型) string: audio/*,video/*,image/*,application/*
 * multiple(是否多传) Boolean
 * url(上传地址) string
*/

import { remove } from "@vue/shared";

var beforeFs:any = [];// 上传前文件
var afterFs:any = [];// 上传成功后文件
var errFs:any = [];// 上传失败文件
var showFs:any = [];// 显示/回显文件
var params:any = new FormData();
var fsBox:any = null;// 文件显示盒子

var accept:any = '*';
var size:any = '5';

var body:any = document.body;

var preicon_url:any = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAACXBIWXMAAAsTAAALEwEAmpwYAAAF42lUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNi4wLWMwMDIgNzkuMTY0NDYwLCAyMDIwLzA1LzEyLTE2OjA0OjE3ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgeG1sbnM6cGhvdG9zaG9wPSJodHRwOi8vbnMuYWRvYmUuY29tL3Bob3Rvc2hvcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgMjEuMiAoV2luZG93cykiIHhtcDpDcmVhdGVEYXRlPSIyMDIxLTA5LTMwVDEzOjM5OjM0KzA4OjAwIiB4bXA6TW9kaWZ5RGF0ZT0iMjAyMS0wOS0zMFQxMzo0MjoyMCswODowMCIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAyMS0wOS0zMFQxMzo0MjoyMCswODowMCIgZGM6Zm9ybWF0PSJpbWFnZS9wbmciIHBob3Rvc2hvcDpDb2xvck1vZGU9IjMiIHBob3Rvc2hvcDpJQ0NQcm9maWxlPSJzUkdCIElFQzYxOTY2LTIuMSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDplMGQ4YzNkMy1iMTY0LWVmNDQtYjBlYS0yMDI4NmYyYzFkZjIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6ZTkxMmRiOTgtYzM5ZC0wMDRlLTg1NzEtZjM5ZTJkMjMwNDJmIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6ZTkxMmRiOTgtYzM5ZC0wMDRlLTg1NzEtZjM5ZTJkMjMwNDJmIj4gPHhtcE1NOkhpc3Rvcnk+IDxyZGY6U2VxPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0iY3JlYXRlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDplOTEyZGI5OC1jMzlkLTAwNGUtODU3MS1mMzllMmQyMzA0MmYiIHN0RXZ0OndoZW49IjIwMjEtMDktMzBUMTM6Mzk6MzQrMDg6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCAyMS4yIChXaW5kb3dzKSIvPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0ic2F2ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6ZTBkOGMzZDMtYjE2NC1lZjQ0LWIwZWEtMjAyODZmMmMxZGYyIiBzdEV2dDp3aGVuPSIyMDIxLTA5LTMwVDEzOjQyOjIwKzA4OjAwIiBzdEV2dDpzb2Z0d2FyZUFnZW50PSJBZG9iZSBQaG90b3Nob3AgMjEuMiAoV2luZG93cykiIHN0RXZ0OmNoYW5nZWQ9Ii8iLz4gPC9yZGY6U2VxPiA8L3htcE1NOkhpc3Rvcnk+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+Yt4OIAAAA2xJREFUWMPNmHtoTmEcx89mMrZsy61hiDS5RLY1QkokhMjwmrnk8ocUMtESKX+Q+M8fQhEp2R8uk15lmyia5Q+XXHZPyq7WXDK7vL6nfquzX7/nnOc556g99emt9/1dvu95nuf3/M5jxWIxy5A4MAtEwDnwAJSCMlACLoHdYC4Y7CN+P0yMM0ARqI/pj6/gBMj8nwKTwWnQHQs2btKfDFXgcvAtFt74C3aGJfCQS6Jm8AgcA2vAHDAbrCW/u6DGxf9KUIEnFYGrwUEwTDPBKvBcEeueX4GFioBnA+zGXaBLiHnHVOBiIcgf+t4KSKZi2gtNBHYw526qeVZI2EujThA5TkfgGcFxUYji+hgtTPczL4FpgrjjmglHgXJQAbI1fVYI+bLcBBYx40aDY++pw68VDNL0LWc5H6sEJoAWZrzXYE05T5gekKrpm81ydoIhksDpzLDFYD3Zx2Cbw/engUCLZso5IpLAjczovuGib3b4dhj6nmK5b0gCzzOjox5Bh9OTGwpGgiaH7w+QDhJBEkjxiLWQ5X4hCXzIjNYpgtm16glop83QRDhLRi8tkUb6tJ9oJZiviDmJ5X4vCSxlRjmKYJUBupjfit2dThurb9RTZegnsIwFm6EQWBew1VIJdFaBL5LAKAu0UiEwF9SCX7TW2umzl02x8zfbtgEUKGJOYbk/S1N8kRkd8VjYaVRKkukJtLJNkkG/pZJtnEusHJa7UhJYwIyKDUtFU4Ayc5jlviUJzGJGDYaFmj/BFAP/Nyz3HklgEvV8zrFMM0ESHVHOd440Td/xwkYaoWoWLngtVgXxQhOaqOl7lflVuHUzE4R/s83g0K+md+B9mj4zhXyrvRrWYuZgl4wxBuspwcCWP/UqnY46VfhXNQZTpktUyLNA951ku+D8CUwMQVi8cO73UKkxeu28rXizywsgbh74IMS1j7mlfl7cSxTnaZRaJMtgM1zWOKfz/Fx9XHcJWEW3D7bYadSKjQVTqbXa73KjoBqb/FweHVDcCEhT1aUppJAupaQR8XP9Ntllyk3GK7DEETdPYZfv9wIzly58vhuI6qR+c70i5mYvkX52ZAoFsNfoS/COOmG72fxIR9Y1sIPaMa94WxQit/oV6Fbn/PrmK0RuCPvexQpZZNtAEig1z7UDTWDf7n4LXtud0j+ias8qDKGeKwAAAABJRU5ErkJggg==";
var delicon_url:any = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAACXBIWXMAAAsTAAALEwEAmpwYAAAFFmlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNi4wLWMwMDIgNzkuMTY0NDYwLCAyMDIwLzA1LzEyLTE2OjA0OjE3ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgeG1sbnM6cGhvdG9zaG9wPSJodHRwOi8vbnMuYWRvYmUuY29tL3Bob3Rvc2hvcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgMjEuMiAoV2luZG93cykiIHhtcDpDcmVhdGVEYXRlPSIyMDIxLTA5LTMwVDEzOjQxOjAxKzA4OjAwIiB4bXA6TW9kaWZ5RGF0ZT0iMjAyMS0wOS0zMFQxMzo0MzowOSswODowMCIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAyMS0wOS0zMFQxMzo0MzowOSswODowMCIgZGM6Zm9ybWF0PSJpbWFnZS9wbmciIHBob3Rvc2hvcDpDb2xvck1vZGU9IjMiIHBob3Rvc2hvcDpJQ0NQcm9maWxlPSJzUkdCIElFQzYxOTY2LTIuMSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo3MzU5NTFjOS00YzI5LTVhNDUtYmFmMC1mY2VkNzJjOTFlMmMiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NzM1OTUxYzktNGMyOS01YTQ1LWJhZjAtZmNlZDcyYzkxZTJjIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6NzM1OTUxYzktNGMyOS01YTQ1LWJhZjAtZmNlZDcyYzkxZTJjIj4gPHhtcE1NOkhpc3Rvcnk+IDxyZGY6U2VxPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0iY3JlYXRlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDo3MzU5NTFjOS00YzI5LTVhNDUtYmFmMC1mY2VkNzJjOTFlMmMiIHN0RXZ0OndoZW49IjIwMjEtMDktMzBUMTM6NDE6MDErMDg6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCAyMS4yIChXaW5kb3dzKSIvPiA8L3JkZjpTZXE+IDwveG1wTU06SGlzdG9yeT4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7jRKxxAAACIUlEQVRYw+2YSygFYRTHPcpCKRJJSnYoyuMqibKRFEssXBaIkjzCylUeyZYsbJQFysKKnYU8wmVh4a2UhJRXxO16jv/oqNM05mVmumW++nVrvnPm/O6d+b7ud4IEQQjSSQLoA8fgEpypcEGfU6BAbz29coXgRfjbGLFKMFowb7itEOySKbQMekAL6JDQBtrBtEzeqRWCS5IiHh25ecDPcj9AjNmCXlZg1cDiqpB8wTA9gmkgB7hkyCb22M1nQTpdd2kgE5RLBMtA1i/xOXTvb8FaIXDHvCi4HcCC34+3FJyDZ/AA7sEbixH3vRvwaDLiPZ9YnU+6Ltb3gTtxz/x5B0NpVUWCCLDJEmdAOIiivdAsxHsWSX6IVKofS5+/ruIVljhuYMVqJYXVeSVpTdvMOkuctFAwVyKYYKZgMxgF8SoSnWCYXhHbBIvZ/JqCXCWLm7NTsJXNnykIDrG4AzsFG9n8roJgD4vz2inYoFHQw+I2HEFH0BF0BB1BR9AR/D+CTWx+X0Gwj8Vt2SnYzOavFQQHWNyJmYJbLHFCZj6ZjqniGFQQzADvFNcpM++S9GsSzTyTxFPLRO1glKQQl87q+Om4qUlwQuWvull0szq3IFirYImkA9FrgVw+Pdafsain/RYCrmSalfWgClSDGgO4KXdMpg2Tpbc/mGFjj6jfaANT7OsdWSjmU+vUan1f6sAC2AGHtHCMcEgbu5fOzHFqtb8Au8n/GBnuh6UAAAAASUVORK5CYII=";
var delicon_url1:any = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAATWSURBVHhe7Z3NahRBFIUzCXGRBwuIILhR0Z0v5dKFWRhEXQiKuBDyWokTvzNzKyQ3M96umZ6e+pkPimmqblXde3K6O2mSzpG4vb09mc/nr2lXtN+0l/Q9WQwekD5P0OTtPX1e0XeaBk/p+E77y/ECHdM+c7gM6hg0OEOLH04fccHh8REHUna+HHoI3Z/4OLO1ukO1o8GXhRgO+q9pzyTgH+tbCeNf+ejOidQs8X4tRFgD41fHxN4sp6xmNpu9IPYjrRsnqlbVTO3n1rWOGznwJe3u/F5HL06kxtB5ghidwk81QXcY3TBCiGv6mqjaqHHlNc9D3Ac+TtJE3YklToht0JwTqWmQ8wRxuqQ91IAOLaDTNMTEbsaJqiWj9sfiJTRg4oQQ14QTqWE753kI6MaJyj2j1li8hAJNnBBLoDonkvO4zvMwQRsMvSNV5UTlmlFbvngJTTRxQoirwonkuFvneVgg5zpRtBOVGznu3nkeLWTihJjYxTmRnKZ1nocFq3WiciGn6Z3n0cImToiJvXcnksN+nedhg2qcqL3JYf/O82gjEyfECpjciexZlvM8bFisE7VXRm7Ti5fQxiZOCHGTOJE9ynaehwSKcaLWti9USBHiJZSIiRNiBY6eOGvW5TwPCe3NiVorY+/yxEsoMRMnhLhRnMgadTvPQ4KTOVFzM/YqX7yEEjVxQojbyInMact5HhJWgUPviFlOVGzG2vWJl1DiJk6ICRIWSkzbzvNQQM516r9O1JgJHdKEeAkVYuKEmECPCqevL+d5KCjHPQ+cqOOMue2Jl1BhJk6ICXZK69t5HgqUIIOviSZkSBfiJVSoxFlUPgJdiZeg4MFO/B9dipdQ4ds4sWvxEgiwkRMP4t1DQuQ4sSTx9DvSJSAxcv4uRfEH9wmcNPj7vPvYad+3iAiw1Z3YTvvRnmxXhQrfxHmeLp1IwRJv0E8YQ+jKiSp0qPOI+2jihBDXvhMpcLDzJB4f6WHCoOukid2mE1UYBW70VEXHJk4IcYunODa1DSgox0Urv0mmr08nqhAKGuV5nsZMnBDi6nciBUi8UZ/nEZOzZr1OVOIUMIrzPIo1cUKIq+/uTMI516ss8RLMadOJSpSEd+I8j+aaOCEmdtlOJMGdO8/DGm04UYmR4CTO82gtEyeEuPKuiSQ0ufM8rFmnE5UICe3FeR6tbeKEELd/J5LA3p3nYY86nKiNSaAI53m0l4kTQtz0TmTD4pznYc8ynaiN2LBI53m0t4kTQtzuncgGxTvPQw45Oe/OiVqYDapwnke5mDghxI3/EwsLVuc8Dzntx4laiAWrdJ5HuZk4IcRt70QWqN55HnJUTbu/O2siCzThPI9yNXFCiJOB8mpjQs5XqSrxEuS8GycqkAlNOs+j3E2cEOJiJxLQvPM81DCOEzVAQBfO86gWEyeEuMdOpKM753moaTMncqBXgP5cjAS0Kl5CtZk4IcRd8nGil9C+oQ15CW3T4iWoUU4Mv/clRi+hfR6+R1r0Il5CtVJz6ERi4vdIE3cxm83e0a6tq3lUq2qm9m/WtY7Fe6Rf0da9Cr4r53lUOxqsdCL9OoXPU9CFOpZDd4N6T3K34iXQQNfES6fPDe09h8u/ctABHc9o6d89PKVv+ZLpA9JH/y7kudMH8Y6O/gHy73z87G8nBAAAAABJRU5ErkJggmdDnqbT9qruS6ztZlg6XDYA4ciHY6gMsm4fCEk3PQlQk+EeZgyVQRpA4t4QhKMsc6x3DQkIe5JH4SUcdXAMWWKxJ7mFgy931vPR/+cgexoAT1E3PQmwIXdfYu75Y+/nw5ZYjTJJc0gIx57l834+BSCzfEAGzJbDZ47hm/Rz58TIBgLurXmWzMsBdVdPk0EaPAKGGYlw1EFw7e7pABnt8Sew54ABb2f3/JGnBAQMiZmxgHBM03OcIjQtIL1DQjjys0HJHVMD0iskwJ5j2swx5VOsSydIT4YDrtWsNCw5yVvdM30G6enpFuHAY0JANpp7ruuBa2Pm2HiCgJwcSh6NCFzT9D0Hn2IlZGlPpQzhSAiY4SXMIBfE9QAJcA3MHBd8QECunD4tDQqcmz3HFQ8QkJ303MKoLeY0rFK6HpqAJIQPaNh7h+Ws/01YXfElzBwJ0hGQBJHAn7gnrqjqMvYcifIRkEShDpCgvuedsarsSwlHhmQEJEOsATIJ4ciMNwHJFKxjSNhzFMSagBSI1iEkhKMwzgSkULiOICEcFTEmIBXidQAJe47K+BKQSgEdQ0I4FGJLQBREdAgJ4VCKKwFREtIRJOw5FGNKQBTFdAAJ4VCOJwFRFrQhJITDIJYExEDUBpCw5zCKIwExEhb4BvCyAwJiFEcCYiAsGI51ByyxDGJJQJRFbQQHIVGO4zocAVEUtjEchEQxlgREWUwncBAS5bgygygI6gwOQqIQU2YQJRGdwrHujk+3KuPMDFIhoHM4mEkqYssMUileJ3AQkso4M4MUCNgZHISkIMbMIIWidQoHISmMNzNIhnDAXySdsarsS/mJe4ZkBCRRrM4zx+ku+XQrMe4EJEEoIBz3D8tZfkGd9R9mkgSFCciOSEA4bk91YClHSHbiT0CuCNQCjnU5wLkJyRUPEJAL4gBP8Yv9ACFJqIGMLyEgZwQGwrF7ehMSYwJYYuUJ7AmOTbmF+q3yfLp1YhdmkI0gHuFgT5J3wGlfTUAOigJLmeJT2jPA2sb0Mh4BEZEe4GAmaYPM9ID0BAchwUMyNSBAOHafVuWGvue15+615fXTAjKCwYA9SXHf1NLcGnNPCcgIcLDc0rD//hjTATLiqQvck3qpuG/RtldMBQjQSPCSZKSs2BaJ49mnAQRoIDgcLLfskJoCECAczUuQmfZqh8X/Iw8PyIyGGbmURECxnWNoQGaEg+WWLkLDAjIzHBtIUG8BNy8tdbEYvMQiHP8HmFrUoTNcBgEaotnTqtyQU5NcxQbNIDTCZSMAtRmq3BomgwAN0E3mOMUF+HRrGEiGAAQIR/eBp1Z55Vb3gDDgeQFfrmYmSdesa0AIR3qgz5Rb90SEv8FxR8JuAQHC0W3PsYcPNdxTSKRLQBjY/cCmXgHUssv+rTtAgAEdNnPw6Vbq8dFZBmFzmR7Y3CuBB09XmaSbDEI4ci2ffz0huatZF4AwcPlmL72DWh8r5x4QYMCm6Tn24AFma/eauwaEcOxZ2e7nQO1d9yRuAeEpZmf+1JGBMXALiUtAGJhUC9tfN3smcQfI7AGxt3z+DDPHxBUgwEC4bw7zbWx7BzA2rsotN4AAA0A4ClkClr5uYuQCEMJR6NgGtwFj5SKTNAdkNsEbeFp9SmAmaQ5JU0AIh7p3YQPOErtmgMwiMMyxDSaaIYZNAAEK66bZa+BfyJTAWDYpt+CAAAUlHBBEoN9xh8cUCgiwuYMLCfKi22mABx80k8AAGVVAt45tsDDgAQiDBAII4Wjg1kZTjhZrc0BGE6yR77qadqSYmwICFIo9hzOEgLE3LbfMABlFIGe+62o5wJ7E7IA0AYRwdOVj08X27gV1QHoXxNQtkw4OzCTq5ZYqIEAhzFLqpB4233avB6caIEABCIe5nW0mAHpELZOoAALcOOGw8S5sVKBXVCCpBqS3DcOcwIkuKtBTKV4FCHCjKqcBPetHgV4O1mJAetmgH0twJacK9HDAFgFCOGh2LQW8eykbEOCG2JBrudD5OEBPZZfqWYAAUyLhcG5q7eV5hSQZEOAGCIe2+zoZz+MBnAQIEI7sFNhJ7LnMRAW8eW0XEG8LTtSZl3WsADCT7B7IVwHxtNCO482lFyjg5WC+CAjhKIgqb1FVwAMkZwHxsDBVpTlYtwq09uIdQICZg0+rurUtduEtPXkECJBWwoH1WPezAb151LjfAhJjfFlEfgAoSTgAIo84BTCTvBJC+HHR8AaQGOMTIvKTiDxvLOzuYzXj+Tl85wqAMskDEXkphPBwBeQzEXnPWDvCYSzwLMODIPk8hPB+iDE+JiJ/ishThgKzrDIUd8ahAZD8LSJPL4C8KiLfG4rMzGEo7sxDA3qS1xdAvhSRt42EJhxGwnLYRwoYZ5KvLAEhHHQxRAHDTHIDiEWJRTgg1uAkqwJGmeSmxNJu0gkHfdtEAWVIHjXphzruCxF5R2FXhENBRA5RroAiJJ+GED7YfpL+u4g8U740IRwV4vFWPQUUepIHIYQXlhVtAXlRRH4pXCbhKBSOt9koUJlJngsh/HYESMUjM8JhE2OOWqlAISTnX1bcPA14VkS+Tngva3lf5cP1pa7KvfB2KmCiwOEl3I8T/PyHiLwZQvh1u5Br3yj8RETeEpEnT1b+l4h8c4DjocmuOCgVUFTg8DLuAskbF/z8bQjh3XNT7n0n/XERee3w918R+U5Efg4h/KO4fg5FBSAKxBiz/fwfP1BW5g9ta0UAAAAASUVORK5CYII=";

// 选择文件 并上传
// function upFile(accept:any='*',size:any,multiple:Boolean=false,url:string='',boxid:any='') {
function upFile(param:Object={}) {
    accept = (param as any).accept||'*';
    size = (param as any).size||'1';
    return new Promise((resolve, reject) => {
        const el:any = document.createElement('input');
        el.type = 'file';
        el.accept = accept||'*';//  声音:audio/*  视频：video/*   图像：image/*
        el.multiple = (param as any).multiple||false;
        // return;
        el.addEventListener('change', (e:any) => {
            beforeFs = el.files;
            // 显示并上传选择的文件
            show_upload_files(resolve,reject,(param as any).url,beforeFs,(param as any).boxid);
            // 上传选择的文件
            // upload(resolve,reject,beforeFs,(param as any).url);
        });
        el.click();
    });
}


// 批量回显
async function show_upload_files(resolve:any,reject:any,url:string,files:any,boxid:any=''){
    fsBox = document.getElementById(boxid);
    let fs:any = files;
    for (let i = 0; i < fs.length; i++) {
        let f:any = fs[i];
        let img_id:string = `img_${new Date().getTime()}`;
        if(f){
            await file_Reader(f,img_id);
            await ajax({
                  method: 'POST',
                  async: true,
                  url: url,
                  data: f,
                  img_id: img_id
            }).then((res:any)=>{
                afterFs.push(JSON.parse(res).filename);
            }).catch((err)=>{
                errFs.push(params);
            })
        }
    }
    resolve(afterFs);
}

//读取一个文件
function file_Reader(f:any,img_id:any){
    return new Promise((resolve, reject) => {
        let fileReader = new FileReader(); // 文件读取
        if(!configAstrict(f)){
            return;
        }
        fileReader.readAsDataURL(f);
        fileReader.onload = () => {
            configImg(f,fileReader.result,img_id);// 配置文件
        }
    })
}

// 配置单位file/img/video/*
function configImg(f:any,fileurl:any,img_id:any){
    let type:any = f.type.split('/')[0]; // 文件类型
    let imgBox:any = document.createElement("div");//单位盒子
    let setBox:any = document.createElement("div");//操作盒子
    let preIcon:any = document.createElement("img");//预览图标
    let delIcon:any = document.createElement("img");//删除图标
    let img:any = null;
    let file:any = null;

    if(type==='image'){
        img = document.createElement("img");
        img.setAttribute('id',img_id);
        img.setAttribute('src',fileurl);
        img.style.cssText="max-width: 144px;max-height: 144px;object-fit:contain;cursor: pointer;";
    }else if(type==='application'){//文件
        file = document.createElement("span");
        file.setAttribute('id','file_');
        file.innerText = f.name;
        file.style.cssText="font-size:12px;padding: 5px;box-sizing: border-box;cursor: pointer;";
    }
    preIcon.setAttribute('src',preicon_url);
    delIcon.setAttribute('src',delicon_url);

    fsBox.style.cssText="display:flex;flex-direction: row;align-items: center;flex-wrap: wrap;";
    imgBox.style.cssText="position: relative;display: flex;align-items: center;justify-content: center;width: 144px;height: 144px;overflow: hidden;border-radius: 6px;border: solid 1px rgb(211, 211, 211); box-sizing: border-box;margin:0 10px 10px 0";
    setBox.style.cssText="opacity: 0;position: absolute;z-index:1;left:0;top:0;width: 144px;height: 144px;background:rgba(0,0,0,0.3);display:flex;flex-direction: row;align-items: center;justify-content: center;";
    preIcon.style.cssText="cursor: pointer;width:20px;height:20px;margin:0 10px;";
    delIcon.style.cssText="cursor: pointer;width:20px;height:20px;margin:0 10px;";

    if(type==='image'){ 
        setBox.appendChild(preIcon);
    }
    setBox.appendChild(delIcon);
    imgBox.appendChild(img||file);
    imgBox.appendChild(setBox); 
    fsBox.appendChild(imgBox);

    imgBox.addEventListener('mouseover',(e:any)=>{
        setBox.style.opacity="1";
    });
    imgBox.addEventListener('mouseout',(e:any)=>{
        setBox.style.opacity="0";
    });
    delIcon.addEventListener('click',(e:any)=>{//删除
        fsBox.removeChild(imgBox);
    });
    preIcon.addEventListener('click',(e:any)=>{//预览
        preview(fileurl||'');
    });
}

 
// 批量上传
async function upload(resolve:any,reject:any,files:any,url:string){
    let fs:any = files;
    for (let i = 0; i < fs.length; i++) {
        let f:any = fs[i];
        if(f){
            await ajax({
                  method: 'POST',
                  async: true,
                  url: url,
                  data: f,
            }).then((res:any)=>{
                afterFs.push(JSON.parse(res).filename);
            }).catch((err)=>{
                errFs.push(params);
            })
        }
    }
    resolve(afterFs);
}

function ajax(opt:any){
    
    return new Promise((resolve, reject) => {
        let xhr:any = XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
            
        params.append('file',opt.data);
    
        if (opt.method.toUpperCase() === 'POST') {
            xhr.open(opt.method, opt.url, opt.async);
            xhr.upload.addEventListener("progress", (e:any)=>{progressFunction(e,opt.img_id)}, false);
            xhr.send(params);
        }
        else if (opt.method.toUpperCase() === 'GET') {
            xhr.open(opt.method, opt.url + '?' + params, opt.async);
            xhr.send(null);
        }
    
        xhr.onreadystatechange = function(){
            if(xhr.readyState==4){
                let status:any = xhr.status;
                if(status>=200&& status<300 || status==304){
                    resolve(xhr.responseText);
                    // opt.success&&opt.success(xhr.responseText,xhr.responseXML);
                }else{
                    reject(xhr.responseText);
                    // opt.error&&opt.error(xhr.responseText);
                }
            }
        }
    })
}

// 预览一张图
function preview(url:any){
    const box:any = document.createElement('div');
    box.style.cssText="position:fixed;z-index:99999999;top:0;right:0;bottom:0;left:0;background:rgba(0,0,0,0.3);display:flex;align-items: baseline;justify-content: center;overflow-y: auto;padding:20px 100px;box-sizing: border-box;overflow-y: auto;";

    const img:any = document.createElement('img');
    img.setAttribute('src',url);
    img.style.cssText="max-width:90%;";

    const delIcon:any = document.createElement('img');
    delIcon.setAttribute('src',delicon_url1);
    delIcon.style.cssText="position: fixed;right:20px;top:20px;width:30px;height:30px;cursor: pointer;";

    box.appendChild(img);
    box.appendChild(delIcon);
    body.appendChild(box);

    box.addEventListener('click',(e:any)=>{
        if(e.target!=box) return;
        box.remove();
    })
    delIcon.addEventListener('click',(e:any)=>{
        box.remove();
    })
}

// 配置限制条件 格式/大小
function configAstrict(f:any){
    let type:any = f.type; // 文件类型
    let isJPG = accept === type || accept==='*' || !accept;
    let exceSize = f.size / 1024 / 1024 <= size;
    if (!isJPG) {
        alter('上传格式不对!','warning');
    }
    if (!exceSize) {
        alter('上传文件大小不能超过 ' + size + 'MB!','warning');
    }
    return isJPG && exceSize;
}

// 主题type primary/success/info/warning/danger
var themes:object = {
    'primary': 'background:#ecf5ff;border: solid 1px #409eff;color:#409eff;',
    'success': 'background:#f0f9eb;border: solid 1px #67c23a;color:#67c23a;',
    'info': 'background:#f4f4f5;border: solid 1px #909399;color:#909399;',
    'warning': 'background:#fdf6ec;border: solid 1px #e6a23c;color:#e6a23c;',
    'danger': 'background:#fef0f0;border: solid 1px #f56c6c;color:#f56c6c;',
}
var alterIdx:number = 0;//弹出提示的总数量
var alterIds:any = [];//弹出框的id s
var top:number = 0;
var alter_style:any = document.createElement('style');
alter_style.setAttribute('id',`keyframes_style`);
alter_style.type = 'text/css';
alter_style.innerHTML  =`.a_box_{animation-fill-mode: forwards;width:300px;height:auto;padding:10px 20px;box-sizing: border-box;border-radius: 6px;position:fixed;z-index:100000;left:50%;margin-left:-150px;}`;
(document as any).getElementsByTagName('head')[0].appendChild(alter_style);
// 弹出提示
function alter(tip:string='',type:string='primary'){

    const aBox_s:any = document.getElementsByClassName(`a_box_`);
    if(aBox_s.length===0){ // 没有弹出框了 最后一个隐藏后会清空
        alterIdx = 0;
        alterIds = [];
        top = 0;
    }else{
        const aBox_:any = aBox_s[aBox_s.length-1];
        top += (aBox_.getBoundingClientRect().height + 20);
    }

    
    // 创建style标签  先判断有没有已经创建过的style id为keyframes_style
    // if(!document.getElementById('keyframes_style')){
        const alterRun =`@keyframes alter_run_${alterIdx}{0%{top: ${top}px;opacity: 0;}100%{top: ${top+60}px;opacity: 1;}}`;
        const alterHid =`@keyframes alter_hid_${alterIdx}{0%{top: ${top+60}px;opacity: 1;}100%{top: ${top}px;opacity: 0;}}`;
        const aler_box_x_t =`#aler_box_${alterIdx}_${top}{top:${top+60}px;${(themes as any)[type]}}`;
        const alterStyle:any = document.getElementById('keyframes_style');
        alterStyle.innerHTML += (alterRun + alterHid + aler_box_x_t);
    // }

    const aBox:any = document.createElement('div');
    const span:any = document.createElement('span');
    
    alterIds.push(`aler_box_${alterIdx}_${top}`);
    aBox.setAttribute('id',`aler_box_${alterIdx}_${top}`);//唯一id
    aBox.setAttribute('class',`a_box_`);//公共样式
    aBox.setAttribute('style',`animation: alter_run_${alterIdx} 0.3s ease-in;`);
    setTimeout(() => {
        const alterIdx:number = alterIds[0].split('_')[2];
        aBox.setAttribute('style',`animation: alter_hid_${alterIdx} 0.3s ease-in;`);
        alterIds.shift();
    }, 2300);
    setTimeout(() => {
        aBox.remove();
        const aBox_s:any = document.getElementsByClassName(`a_box_`);
        if(aBox_s.length===0){
            alter_style.innerHTML  = `.a_box_{animation-fill-mode: forwards;width:300px;height:auto;padding:10px 20px;box-sizing: border-box;border-radius: 6px;position:fixed;z-index:100000;left:50%;margin-left:-150px;}`;
        }
    }, 2550);
    span.innerHTML = tip||'';

    aBox.appendChild(span);
    body.appendChild(aBox);
    alterIdx += 1;
}

function progressFunction(evt:any,img_id:any){
    // 进度相关
    const proBox:any = document.createElement("div");
    const proBar:any = document.createElement("div");
    
    proBox.setAttribute('id','progress_bar_box');
    proBox.style.cssText="opacity: 0;position: absolute;z-index:1;left:0;top:0;width: 144px;height: 144px;background:rgba(0,0,0,0.3);display:flex;flex-direction: row;align-items: center;justify-content: center;";
    proBar.setAttribute('id','progress_bar');
    proBar.style.cssText="color:#fff;";
    proBox.appendChild(proBar);
    const imgBox:any = document.getElementById(img_id);
    imgBox.parentNode.insertBefore(proBox,imgBox.nextElementSibling);
    if (evt.lengthComputable){
        console.log(evt);
        const pro:number = Math.round(evt.loaded / evt.total * 100);
        proBar.innerHTML = pro + "%";
        if(pro>=100){
            proBox.remove();
        }
    }
}

export default upFile;
