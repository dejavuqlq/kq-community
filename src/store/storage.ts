export default {
    state: {
        newToken: ""
    },
    mutations: {
        setNewToken(state: any, newToken: string) {
            state.newToken = newToken
        },
    },
    actions: { // Action 提交的是 mutation，而不是直接变更状态。 可以包含任意异步操作。
        setNewToken(context: { commit: (arg0: string, arg1: string) => void, state: any }, val = '') {
            new Promise(resolve => {
                context.commit("setNewToken", val);
                resolve();
            });
        }
    }
}