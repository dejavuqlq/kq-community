import { createStore } from 'vuex'
import storage from './storage';
// url对应的tab的名字
const urlName = {
  "/tab1":"首页",
  "/tab2":'修改密码',
  "/tab3":'管理员管理',
  "/tab4":'导入用户',
  "/tab5":'用户管理',
  "/tab6":'图书列表',
};
export default createStore({
  state: {  // 单一状态树
    version:'V1.1.2', // 版本号
    systemName:"柯桥社区管理系统", // -------------后台的名称
    username:'', // 登录的用户名
    userid:'',
    power:"0", // 登录的用户的权限  0 普通用户  1 管理员  2 超级管理员
    token:'',  // 用户权限
    urlList:{  // 已打开的页面的数量
      "/tab1":1,   // 首页
    },
    urlName,
    menu:[], // 用户的权限菜单
    allPowerMenu:[] , // 所有的权限列表
    pageAuthoritys:{},
    parentHeight:'',
    Breadcrumbs:'系统管理/管理员管理',
    org_id: "",//所属组织id
    orgList: [],//组织架构
    asideIndex:0,// store的默认选中项
  },
  mutations: { // 更改 Vuex 的 store 中的状态
    asideIndex(state, asideIndex){ state.asideIndex = asideIndex },
    // 更新urlList
    updateUrl(state, urlList){ state.urlList = urlList },
    // 更新urlList
    Breadcrumbs(state, Breadcrumbs){ state.Breadcrumbs = Breadcrumbs },
    setUserName(state,username){  state.username = username },
    setUserId(state,userid){   state.userid = userid },
    setPower(state,power){  state.power = power;  },
    setToken(state,token){   state.token = token; },
    setPH(state,parentHeight){   state.parentHeight = parentHeight; },
    setMenu(state,menu){   state.menu = menu; },
    setPageAuthoritys(state,pageAuthoritys){   state.pageAuthoritys = pageAuthoritys; },
    setAllPowerMenu(state,allPowerMenu){   state.allPowerMenu = allPowerMenu; },
    setOrg(state,org_id){   state.org_id = org_id; },
    setOrgList(state,orgList){   state.orgList = orgList; }
  },
  getters:{
    getMenu(state){
      return state.menu;
    },
  },
  actions: { // Action 提交的是 mutation，而不是直接变更状态。 可以包含任意异步操作。
    setAsideIndex( {state,commit },val=0 ){
      new Promise(resolve => {
        commit("asideIndex", val);
        resolve();
      });
    },

    setPHAction({state,commit },val='0'){
      new Promise(resolve => {
        commit("setPH", val);
        resolve();
      })
    },
    updateUrl( {state,commit },obj={} ){  // obj 为传递过来的参数
      new Promise(resolve => {
        commit("updateUrl", obj);
        resolve();
      })
    },
    setUserNameAction( {state,commit },name='管理中心/首页'){  // obj 为传递过来的参数
      new Promise(resolve => {
        commit("setUserName", name);
        resolve();
      })
    },
    // 更改用户权限
    setPowerAction({state,commit },val='0'){
      new Promise(resolve => {
        commit("setPower", val);
        resolve();
      })
    },
    // 更改用户id
    setUserIdAction({state,commit },val=''){
      new Promise(resolve => {
        commit("setUserId", val);
        resolve();
      })
    },
    // 用户token
    setTokenAction({state,commit },val='0'){
      new Promise(resolve => {
        commit("setToken", val);
        resolve();
      })
    },
    setMenuAction({state,commit },val=[]){
      new Promise(resolve => {
        commit("setMenu", val);
        resolve();
      })
    },
    setPageAuAction({state,commit },val=[]){//配置页面内权限
      new Promise(resolve => {
        commit("setPageAuthoritys", val);
        resolve();
      })
    },
    setBreadcrumbs({state,commit },val=''){
      new Promise(resolve => {
        commit("Breadcrumbs", val);
        resolve();
      })
    },
    setOrgAction({state,commit },val=""){
      new Promise(resolve => {
        commit("setOrg", val);
        resolve();
      })
    },
    setOrgListAction({state,commit },val=[]){
      new Promise(resolve => {
        commit("setOrgList", val);
        resolve();
      })
    },
    // 所有的权限列表
    setAllPowerMenuAction({state,commit },val=[]){
      new Promise(resolve => {
        commit("setAllPowerMenu", val);
        resolve();
      })
    },
    loginOut({state,commit },val=''){
      new Promise(resolve => {
        commit("setAllPowerMenu", []);
        commit("setOrg", "");
        commit("setOrgList", []);
        commit("setToken", '0');
        commit("setUserName", '');
        commit("asideIndex", 0);
        commit("Breadcrumbs", '系统管理/管理员管理');
        resolve();
      })
    }
  },
  modules: { // 将 store 分割成模块（module）
    storage: storage
  }
})
