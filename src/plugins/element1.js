import {
    ElButton,
    ElForm,
    ElFormItem,
    ElHeader,
    ElImage,
    ElInput,
    ElInputNumber,
    ElSwitch,
    ElTabPane,
    ElTable,
    ElTableColumn,
    ElTabs,
    ElCollapse,
    ElCollapseItem,
    ElContainer,
    ElAside, ElMain,
    ElMenu,
    ElMenuItem, ElBreadcrumb, ElDatePicker, ElTimePicker, ElTimeSelect,
    ElBreadcrumbItem, ElSelect, ElOption, ElDialog, ElDrawer, ElCheckbox, ElPageHeader,
    ElPagination,
    ElPopconfirm,
    ElPopover,
    ElPopper,
    ElCheckboxButton,
    ElCheckboxGroup, ElRadio,
    ElRadioButton,
    ElRadioGroup, ElCollapseTransition, ElInfiniteScroll,
    ElLoading,
    ElMessage,
    ElMessageBox,
    ElNotification, ElUpload,
    ElDropdown,
    ElDropdownItem,
    ElDropdownMenu,
    ElOptionGroup,
    ElAutocomplete,
    ElCascader,
    ElRate,
    ElTag,
    ElRow,
    ElCol,
} from 'element-plus'

const plugins = [
    ElInfiniteScroll,
    ElLoading,
    ElMessage,
    ElMessageBox,
    ElNotification,
];
// 是指默认语言为中文
import lang from 'element-plus/lib/locale/lang/zh-cn'
import locale from 'element-plus/lib/locale'
import 'dayjs/locale/zh-cn'
// 设置语言
locale.use(lang);
export default (app) => {
    app.use(ElButton)
        .use(ElForm)
        .use(ElFormItem)
        .use(ElImage)
        .use(ElInputNumber)
        .use(ElInput)
        .use(ElHeader)
        .use(ElAutocomplete)
        .use(ElOptionGroup)
        .use(ElSwitch)
        .use(ElTabPane)
        .use(ElTable)
        .use(ElTableColumn)
        .use(ElTabs)
        .use(ElCollapse)
        .use(ElCollapseItem)
        .use(ElContainer)
        .use(ElAside)
        .use(ElMain)
        .use(ElMenu)
        .use(ElMenuItem)
        .use(ElBreadcrumb)
        .use(ElBreadcrumbItem)
        .use(ElSelect)
        .use(ElOption)
        .use(ElDialog)
        .use(ElDrawer)
        .use(ElUpload)
        .use(ElDatePicker)
        .use(ElTimePicker)
        .use(ElTimeSelect)
        .use(ElCheckbox)
        .use(ElCheckboxButton)
        .use(ElPageHeader)
        .use(ElPagination)
        .use(ElPopconfirm)
        .use(ElPopover)
        .use(ElPopper)
        .use(ElRadio)
        .use(ElRadioButton)
        .use(ElRadioGroup)
        .use(ElCheckboxGroup)
        .use(ElCollapseTransition)
        .use(ElDropdown)
        .use(ElDropdownItem)
        .use(ElDropdownMenu)
        .use(ElCascader)
        .use(ElRate)
        .use(ElTag)
        .use(ElRow)
        .use(ElCol)
    plugins.forEach(plugin => {
        app.use(plugin)
    })
}
