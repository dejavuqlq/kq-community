"use strict";

import axios from "axios";

// if ( process.env.NODE_ENV ==='production' ) {//生产环境，测试环境
//   axios.defaults.baseURL = process.env.VUE_APP_BASE_URL;
// } else {
//   //serve 开发环境
//   axios.defaults.baseURL = '/';
// }
// Full config:  https://github.com/axios/axios#request-config
// axios.defaults.baseURL = process.env.baseURL || process.env.apiUrl || '';
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
axios.defaults.headers.post['Content-Type'] = 'application/json';
// axios.defaults.headers['NODEWEBKIT'] = 'zh8848';
//- http://kq-community.server.zhhost.top/manager
let config = {
  baseURL: 'http://kq-community.server.zhhost.top/',
  // baseURL: process.env.NODE_ENV === 'development' ? '/' : 'http://kq-community.server.zhhost.top/',
  timeout: 60 * 1000, // Timeout
  // withCredentials: true, // Check cross-site Access-Control
};

const _axios = axios.create(config);

_axios.interceptors.request.use(
    function(config) {
      // Do something before request is sent
      return config;
    },
    function(error) {
      // Do something with request error
      return Promise.reject(error);
    }
);

// Add a response interceptor
_axios.interceptors.response.use(
    function(response) {
      // Do something with response data
      return response;
    },
    function(error) {
      // Do something with response error
      return Promise.reject(error);
    }
);

export default {
  install:function(app:any, options:any) {
    app.config.globalProperties.axios = _axios;
    // 添加全局的方法
    app.config.globalProperties.$translate = (key: any) => {
      // return key.split('.').reduce((o, i) => {
      //   if (o) return o[i]
      // }, i18n)
      return key
    }
  }
}