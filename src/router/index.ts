import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Datas from '../views/datas/index.vue'
import Datastest from '../views/datas/Datastest.vue'
// 默认路由 普通用户
const routes = [
    { path: '/', name: 'Login', component: Login },
    // { path: '/', name: 'Datas', component: Datas },
    {
        path: '/Home', name: "Home", component: Home,
        children: [
            { path: '/systemmanage', name: "systemmanage", component: () => import('../views/systemmanage/list.vue') },
            { path: '/usersmanage', name: "usersmanage", component: () => import('../views/usersmanage/list.vue') },
            { path: '/warningmanage', name: "warningmanage", component: () => import('../views/warningmanage/list.vue') },
            { path: '/housemanage', name: "housemanage", component: () => import('../views/housemanage/list.vue') },
            { path: '/carmanage', name: "carmanage", component: () => import('../views/carmanage/list.vue') },
            { path: '/partybuildchmanage', name: "partybuildchmanage", component: () => import('../views/partybuildchmanage/list.vue') },
            { path: '/securitymanage', name: "securitymanage", component: () => import('../views/securitymanage/list.vue') },
            { path: '/propertymanage', name: "propertymanage", component: () => import('../views/propertymanage/list.vue') },
            { path: '/antiepidemicmanage', name: "antiepidemicmanage", component: () => import('../views/antiepidemicmanage/list.vue') },
            { path: '/activitymanage', name: "activitymanage", component: () => import('../views/activitymanage/list.vue') },
            { path: '/filemanage', name: "filemanage", component: () => import('../views/filemanage/list.vue') },
        ]
    },
    {
        path: '/Datas', name: "Datas", component: Datas,
        children: []
    },
    {
        path: '/Datastest', name: "Datastest", component: Datastest,
        children: []
    },
];
// 初始化 默认路由
const router = createRouter({
    history: createWebHashHistory(),
    routes
});
export default router
