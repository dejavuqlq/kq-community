const path = require('path')

module.exports = {
    pluginOptions: { // 第三方插件配置
        'style-resources-loader': {
            preProcessor: 'scss',
            patterns: [path.resolve(__dirname, './src/element-variables.scss')] // scss所在文件路径
        }
    },
    lintOnSave: false,
    publicPath: './',
    // 开启热更新
    chainWebpack: config => {
        config.resolve.symlinks(true)
        config.plugin('html').tap(args => {
            args[0].title = "柯桥社区管理"; // 修改html 的title
            return args;
        })
    },
    // 开发配置项
    devServer: {
        open: true,
        host: 'localhost',
        port: 8080,
        https: false,
        hotOnly: false,
        proxy: { // 配置跨域
            '/store': {
                //要访问的跨域的api的域名
                target: 'http://kq-community.server.zhhost.top/',
                ws: true,
                changOrigin: true,
                pathRewrite: {
                    '^/store': '/store/'
                }
            }
        },
        before: () => {
        }
    },
    // css解析模式
    css: {
        loaderOptions: {
            scss: {
                data: `@import "~@/element-variables.scss";`
            }
        }
    },
    configureWebpack: config => {
        config.externals = {
            "BMap": "BMap",
            // "BMapGL": "BMapGL",
            // "AMap": "AMap",
            // "TMap": "TMap",
        }
    },

}